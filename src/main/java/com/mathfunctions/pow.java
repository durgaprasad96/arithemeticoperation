package com.mathfunctions;

public class pow {
    public static int power(int a,int b)
    {
        if(b<=0)
            return 1;
        else
            return a * power(a,--b);
    }
}
